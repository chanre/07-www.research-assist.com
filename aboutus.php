<!DOCTYPE html>
<html lang="zxx">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title>Research Assist - Concept to publication</title>

<link rel="preconnect" href="https://fonts.gstatic.com/">
<link href="https://fonts.googleapis.com/css2?family=PT+Serif:wght@400;700&amp;family=Poppins:wght@200;300;400;500;600;700;800&amp;display=swap" rel="stylesheet">

<link rel="shortcut icon" href="assets/img/favicon.png" type="image/png">

<link href="assets/css/bootstrap.min.css" rel="stylesheet">

<link href="assets/css/jquery-ui-min.css" rel="stylesheet">

<link href="assets/css/line-awesome.min.css" rel="stylesheet">
<link href="assets/css/remixicon.css" rel="stylesheet">

<link href="assets/css/animate.min.css" rel="stylesheet">

<link href="assets/css/swiper-min.css" rel="stylesheet">

<link href="assets/css/magnific-popup.css" rel="stylesheet">

<link href="assets/css/style.css" rel="stylesheet">


<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-TB1EHXL2BC"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-TB1EHXL2BC');
</script>
</head>
<body>

<div class="preloader js-preloader">
<img src="assets/img/preloader.gif" alt="Image">
</div>


<div class="page-wrapper">

<?php include ('layout/header.php'); ?>
<section class="breadcrumb-wrap bg-f br-bg-1">
<div class="overlay op-6 bg-black"></div>
<div class="container">
<div class="row">
<div class="col-xl-8 offset-xl-2 col-lg-10 offset-lg-1 col-md-10 offset-md-1">
<div class="breadcrumb-title">
<h2>About us</h2>
</div>
</div>
</div>
</div>
</section>



<section class="about-wrap  ptb-100">

<div class="about-content">
<div class="section-title text-center style1">

<h2>Welcome to Research Assist</h2>
<p>Welcome to research-assist, your online partner providing end-to-end solution for publishing manuscript and scientific contents. Our team comprises of statistical analysts, medical specialists, research professionals and IT experts with years of experience in editorial process management and scientific publishing. Our motto is to provide excellent service, without compromising on the quality or accuracy.</p>
<div class="about-subpara">
<!-- <h5><span> <img src="assets/img/about/goal.svg" alt="Image"></span>Ecour Is The Right Place Where You Can Achieved</h5> -->
<p>Meticulous statistical analysis, data interpretation,and presentation of the content are crucial for establishing the success of a research. Our knowledge resources can meet your research needs, thereby to help you in achieving the publication goals.
We hope to serve you at the best through the various services offered. </p>
<p>We comply with ethical publication practices and strive to provide the best services by understanding the customer needs. We cater to various global, academic, scientific and pharma giants to provide high-quality services</p>
</div>

</div>
</div>

<div class="about-img">
<!-- <img src="assets/img/about/about-10.png" alt="Image"> -->
</div>
</section>






<?php include('layout/footer.php'); ?>


</div>


<a href="#" class="back-to-top bounce"><i class="las la-arrow-up"></i></a>


<script src="assets/js/jquery.min.js"></script>

<script src="assets/js/jquery-ui.min.js"></script>

<script src="assets/js/bootstrap.bundle.min.js"></script>
<script src="assets/js/bootstrap-validator.js"></script>
<script src="assets/js/form-validation.js"></script>

<script src="assets/js/swiper-min.js"></script>

<script src="assets/js/jquery-magnific-popup.js"></script>

<script src="assets/js/countdown.js"></script>

<script src="assets/js/main.js"></script>
</body>

</html>