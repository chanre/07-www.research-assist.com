
<!DOCTYPE html>
<html lang="zxx">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title>Contact - Research Assist</title>

<link rel="preconnect" href="https://fonts.gstatic.com/">
<link href="https://fonts.googleapis.com/css2?family=PT+Serif:wght@400;700&amp;family=Poppins:wght@200;300;400;500;600;700;800&amp;display=swap" rel="stylesheet">

<link rel="shortcut icon" href="assets/img/favicon.png" type="image/png">

<link href="assets/css/bootstrap.min.css" rel="stylesheet">

<link href="assets/css/jquery-ui-min.css" rel="stylesheet">

<link href="assets/css/line-awesome.min.css" rel="stylesheet">
<link href="assets/css/remixicon.css" rel="stylesheet">

<link href="assets/css/animate.min.css" rel="stylesheet">

<link href="assets/css/swiper-min.css" rel="stylesheet">

<link href="assets/css/magnific-popup.css" rel="stylesheet">

<link href="assets/css/style.css" rel="stylesheet">
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-TB1EHXL2BC"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-TB1EHXL2BC');
</script>
</head>
<body onload="generateCaptcha()">

<div class="preloader js-preloader">
<img src="assets/img/preloader.gif" alt="Image">
</div>


<div class="page-wrapper">

<?php include('layout/header.php'); ?>



<section class="breadcrumb-wrap bg-f br-bg-1">
<div class="overlay op-6 bg-black"></div>
<div class="container">
<div class="row">
<div class="col-xl-8 offset-xl-2 col-lg-10 offset-lg-1 col-md-10 offset-md-1">
<div class="breadcrumb-title">
<h2>Contact us</h2>
<ul class="breadcrumb-menu">
<li><a href="index.html">Home </a></li>
<li>Contact Us</li>
</ul>
</div>
</div>
</div>
</div>
</section>


<section class="contact-wrap pt-100 pb-70">
<div class="container">
<div class="row justify-content-md-center">
<div class="col-lg-4 col-md-6">
<div class="contact-address">
<div class="contact-icon">
<i class="ri-map-pin-fill"></i>
</div>
<div class="contact-info">
<h5>Address</h5>
<p class="mb-0">No. 414/65, 20th Main,
West of Chord road,
1st Block, Rajajinagar, Bangalore-10</p>
</div>
</div>
</div>
<div class="col-lg-4 col-md-6">
<div class="contact-address">
<div class="contact-icon">
<i class="ri-phone-line"></i>
</div>
<div class="contact-info">
<h5>Phone Number</h5>
<p class="mb-0"><a href="tel:99762236473">080-42516636 / 95350 56289</a>
</p>
</div>
</div>
</div>
<div class="col-lg-4 col-md-6">
<div class="contact-address">
<div class="contact-icon">
<i class="ri-mail-send-line"></i>
</div>
<div class="contact-info">
<h5>Email Address</h5>
<p class="mb-0">marketing@research-assist.com</a>
</p>
</div>
</div>
</div>
</div>
</div>
</section>

<div class="contact-form_wrap pt-20 pb-100">
<div class="container">
<div class="row">
<div class="col-lg-12">
<div class="section-title text-center mb-40 style1">
<span><h2>Contact Us</h2></span>

</div>
</div>
<div class="col-lg-12">
<div class="contact-form">
<form class="form-wrap"  method="POST" onsubmit="return CheckValidCaptcha()" action="mail.php" id="contact-form">
<div id="message" class="alert alert-danger alert-dismissible fade"></div>
<div class="row">
		<div class="col-lg-6">
			<div class="form-group">
				
				<input type="text" name="name" id="name" class="form-control" placeholder="Enter your name .....">
			</div>
		</div>
		<div class="col-lg-6">
			<div class="form-group">
				<input type="text" id="mobile" name="mobile" placeholder="Contact no">
			</div>
		</div>
		<div class="col-lg-6">
			<div class="form-group">
				<input type="text" name="email" class="form-control" placeholder="Enter your email">
			</div>
		</div>
		
		
		<div class="col-lg-6">
			<div class="form-group">
				<input type="text" name="subject" class="form-control" placeholder="Enter your subject">
			</div>
		</div>
		<div class="col-lg-12">
			<div class="form-group v1">
				<textarea name="message" class="form-control" placeholder="Enter your message"></textarea>
			</div>
		</div>
		 <div class="offset-4 col-lg-14 col-md-14">
                 <div class=" v1">     
                      <input type="text" id="mainCaptcha"  readonly="readonly" style="background-image: linear-gradient(#00adff6b, #3ee6df36); text-align: center;font-size: 18px;" />
                      <img src="assets/img/refresh1.png" onclick="generateCaptcha();">
                      <span id="error" style="color:red"></span>
                      <span id="success" style="color:green"></span><input type="text" id="txtInput" placeholder="Enter the captcha here..." /> 
                                        <span id="error" style="color:red"></span></td>
                                         <span id="success" style="color:green"></span><br>  
                </div> 
        </div> 

		<div class="col-lg-12" align="center"><br>
			<button type="submit" name="submit" onclick="CheckValidCaptcha()" class="btn v1 d-block ">Send Message</button>
		</div>
</div>
</form>
</div>
</div>
</div>
</div>
</div>


<?php include('layout/footer.php'); ?>

</div>


<a href="#" class="back-to-top bounce"><i class="las la-arrow-up"></i></a>


<script src="assets/js/jquery.min.js"></script>

<script src="assets/js/jquery-ui.min.js"></script>

<script src="assets/js/bootstrap.bundle.min.js"></script>
<script src="assets/js/bootstrap-validator.js"></script>
<script src="assets/js/form-validation.js"></script>

<script src="assets/js/swiper-min.js"></script>

<script src="assets/js/jquery-magnific-popup.js"></script>

<script src="assets/js/countdown.js"></script>

<script src="assets/js/main.js"></script>
</body>
<script type="text/javascript">
    function generateCaptcha()
         {
             var alpha = new Array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');
             var i;
             for (i=0;i<5;i++){
               var a = alpha[Math.floor(Math.random() * alpha.length)];
               var b = alpha[Math.floor(Math.random() * alpha.length)];
               var c = alpha[Math.floor(Math.random() * alpha.length)];
               var d = alpha[Math.floor(Math.random() * alpha.length)];
               var e = alpha[Math.floor(Math.random() * alpha.length)];
              }
            var code = a + '' + b + '' + '' + c + '' + d +''+e;
            document.getElementById("mainCaptcha").value = code
          }
          function CheckValidCaptcha(){
             
              var string1 = removeSpaces(document.getElementById('mainCaptcha').value);
              var string2 = removeSpaces(document.getElementById('txtInput').value);
              if (string1 == string2){
         document.getElementById('success').innerHTML = "Captcha is validated Successfully";
                return true;
              }
              else{       
         document.getElementById('error').innerHTML = "Please enter a valid captcha."; 
                return false;
         
              }
          }
          function removeSpaces(string){
            return string.split(' ').join('');
          }
          
</script>

</html>