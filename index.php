<!DOCTYPE html>
<html lang="zxx">
<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title>Research Assist - Concept to publication</title>

<link rel="preconnect" href="https://fonts.gstatic.com/">
<link href="https://fonts.googleapis.com/css2?family=PT+Serif:wght@400;700&amp;family=Poppins:wght@200;300;400;500;600;700;800&amp;display=swap" rel="stylesheet">

<link rel="shortcut icon" href="assets/img/favicon.png" type="image/png">

<link href="assets/css/bootstrap.min.css" rel="stylesheet">

<link href="assets/css/jquery-ui-min.css" rel="stylesheet">

<link href="assets/css/line-awesome.min.css" rel="stylesheet">
<link href="assets/css/remixicon.css" rel="stylesheet">
<link href="assets/css/flaticon.html" rel="stylesheet">

<link href="assets/css/animate.min.css" rel="stylesheet">

<link href="assets/css/swiper-min.css" rel="stylesheet">

<link href="assets/css/magnific-popup.css" rel="stylesheet">

<link href="assets/css/nice-select.html" rel="stylesheet">

<link href="assets/css/style.css" rel="stylesheet">
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-TB1EHXL2BC"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-TB1EHXL2BC');
</script>
</head>
<body>
  <script type="text/javascript" src="https://code.jquery.com/jquery-1.8.2.js"></script>
  <style type="text/css">


.card{
  height: 400px;
  border-radius: 10px;
background: #badaf2;
box-shadow:  5px 5px 10px #9cb7cb,
            -5px -5px 10px #d8fdff;
}
#overlay {
top: 0;
left: 0;
width: 100%;
height: 100%;
background-color: #000;
filter:alpha(opacity=70);
-moz-opacity:0.7;
-khtml-opacity: 0.7;
opacity: 0.7;
z-index: 100;
display: none;
}
.cnt223 a{
text-decoration: none;
}
.popup{
width: 100%;
margin-top: -50px;
display: none;
position: fixed;
z-index: 101;
}
.cnt223{
min-width: 600px;
width: 600px;
min-height: 150px;
margin: 100px auto;
background: #f3f3f3;
position: relative;
z-index: 103;
padding: 15px 35px;
border-radius: 5px;
box-shadow: 0 2px 5px #000;
}
@media only screen and (max-width: 600px) {
 .cnt223{
min-width: auto;
width: auto;
min-height: 150px;
margin: 100px auto;
background: #f3f3f3;
position: relative;
z-index: 103;
padding: 15px 35px;
border-radius: 5px;
box-shadow: 0 2px 5px #000;
}
}
.cnt223 p{
clear: both;
color: #555555;
/* text-align: justify; */
font-size: 20px;
font-family: sans-serif;
}
.cnt223 p a{
color: #d91900;
font-weight: bold;
}
.cnt223 .x{
float: right;
height: 35px;
left: 22px;
position: relative;
top: -25px;
width: 34px;
}
.cnt223 .x:hover{
cursor: pointer;
}

.button,.mybtn a {
  background-color: #004A7F;
  -webkit-border-radius: 10px;
  border-radius: 10px;
  border: none;
  color: #FFFFFF;
  cursor: pointer;
  display: inline-block;
  font-family: Arial;
  font-size: 20px;
  padding: 5px 10px;
  text-align: center;
  text-decoration: none;
  -webkit-animation: glowing 1500ms infinite;
  -moz-animation: glowing 1500ms infinite;
  -o-animation: glowing 1500ms infinite;
  animation: glowing 1500ms infinite;
}
@-webkit-keyframes glowing {
  0% { background-color: #B20000; -webkit-box-shadow: 0 0 3px #B20000; }
  50% { background-color: #FF0000; -webkit-box-shadow: 0 0 40px #FF0000; }
  100% { background-color: #B20000; -webkit-box-shadow: 0 0 3px #B20000; }
}

@-moz-keyframes glowing {
  0% { background-color: #B20000; -moz-box-shadow: 0 0 3px #B20000; }
  50% { background-color: #FF0000; -moz-box-shadow: 0 0 40px #FF0000; }
  100% { background-color: #B20000; -moz-box-shadow: 0 0 3px #B20000; }
}

@-o-keyframes glowing {
  0% { background-color: #B20000; box-shadow: 0 0 3px #B20000; }
  50% { background-color: #FF0000; box-shadow: 0 0 40px #FF0000; }
  100% { background-color: #B20000; box-shadow: 0 0 3px #B20000; }
}

@keyframes glowing {
  0% { background-color: #B20000; box-shadow: 0 0 3px #B20000; }
  50% { background-color: #FF0000; box-shadow: 0 0 40px #FF0000; }
  100% { background-color: #B20000; box-shadow: 0 0 3px #B20000; }
}
.poster img{
  max-width: 100%;
  /*margin-top: -100px;*/
}

</style>
<script type='text/javascript'>
$(function(){
var overlay = $('<div id="overlay"></div>');
overlay.show();
overlay.appendTo(document.body);
$('.popup').show();
$('.close').click(function(){
$('.popup').hide();
overlay.appendTo(document.body).remove();
return false;
});
$('.x').click(function(){
$('.popup').hide();
overlay.appendTo(document.body).remove();
return false;
});
});
</script>
<!--
  <div class='popup'> 
      <div class='cnt223'>
          <p>
          <a href='' class="close button">X</a>
          </p>
          <div class="poster">
              <a href="https://www.youtube.com/watch?v=iSi0vQG-DIA"><img src="uploads/arthritisday.png"></a>
              <div class="mybtn">
                <a href="https://www.youtube.com/watch?v=iSi0vQG-DIA" class="btn">View details</a>
              </div>
              
          </div>
      </div>  
    </div>  -->
<div class="preloader js-preloader">
<img src="assets/img/preloader.gif" alt="Image">
</div>


<div class="page-wrapper">

	<?php include('layout/header.php'); ?>





<div class="hero-wrap style2">
<div class="hero-slider-two swiper-container">
<div class="swiper-wrapper">

<div class="swiper-slide">
<div class="hero-slider-item bg-f hero-bg-5">
<div class="container">
<div class="row">
<div class="col-lg-7 offset-lg-5">
<div class="hero-content">
<div class="hero-shape-2 md-none">
<img src="assets/img/hero/dot-shape-2.png" alt="Image">
</div>
<div class="hero-shape-3 md-none">
<img src="assets/img/hero/dot-shape-3.png" alt="Image">
</div>
<div class="hero-shape-4 md-none">
<img src="assets/img/hero/circle-shape-2.png" alt="Image">
</div>
<h1>Online Data Management</h1>
<p>Tired of writing your data on hardcopy, well we are here to provide you with an online platform to record your data.</p>
<div class="hero-btn">
<!-- <a href="course.html" class="btn v1">View Demo</a> -->
<!-- <a href="login.html" class="btn v2">Request a Quote</a> -->
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<div class="swiper-slide">
<div class="hero-slider-item bg-f hero-bg-4">
<div class="container">
<div class="row">
<div class="col-lg-7 offset-lg-5">
<div class="hero-content">
<div class="hero-shape-2 md-none">
<img src="assets/img/hero/dot-shape-2.png" alt="Image">
</div>
<div class="hero-shape-3 md-none">
<img src="assets/img/hero/dot-shape-3.png" alt="Image">
</div>
<div class="hero-shape-4 md-none">
<img src="assets/img/hero/circle-shape-2.png" alt="Image">
</div>
<h1>Manuscript / Thesis / Report Editing</h1>
<p>Get help with manuscripts writing, report preparation and English language style and editing.</p>
<div class="hero-btn">
<!-- <a href="course.html" class="btn v1">View Courses</a> -->
<!-- <a href="login.html" class="btn v2"> Join For Free</a> -->
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<div class="swiper-slide">
<div class="hero-slider-item bg-f hero-bg-1">
<div class="container">
<div class="row">
<div class="col-lg-7 offset-lg-5">
<div class="hero-content">
<div class="hero-shape-2 md-none">
<img src="assets/img/hero/dot-shape-2.png" alt="Image">
</div>
<div class="hero-shape-3 md-none">
<img src="assets/img/hero/dot-shape-3.png" alt="Image">
</div>
<div class="hero-shape-4 md-none">
<img src="assets/img/hero/circle-shape-2.png" alt="Image">
</div>
<h1>Research Designing and Data Analysis Support</h1>
<p>Use our expertise and skills in ensuring a robust study design using data directed to answer your scientific/ clinical questions.</p>
<div class="hero-btn">
<!-- <a href="course.html" class="btn v1">View Courses</a> -->
<!-- <a href="login.html" class="btn v2"> Join For Free</a> -->
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<div class="hero-pagination"></div>
</div>
</div>


<section class="about-wrap  ptb-100">
<div class="container">
	<div class="row">
		<div class="col-md-5"><div class="about-img">
<img src="assets/img/about/about.jpg" alt="Image">
</div></div>

<div class="col-md-7"><div class="about-content">
<div class="section-title text-left style1">
<span>About us</span>
<h2>Welcome To Research Assist</h2>
<p>Welcome to research-assist, your online partner providing end-to-end solution for publishing manuscript and scientific contents. Our team comprises of statistical analysts, medical specialists, research professionals and IT experts with years of experience in editorial process management and scientific publishing. Our motto is to provide excellent service, without compromising on the quality or accuracy.</p>


<a href="aboutus.php" class="btn v1"> <i class="ri-eye-line"></i> View More</a>
</div>
</div></div>

		
</div>
</div>
</section>


<div class="promo-banner-wrap style4 promo-bg-1 bg-f ptb-100">
<div class="overlay bg-midnight op-8"></div>
<div class="promo-circle-shape md-none">
<img src="assets/img/promo/promo-circle-shape.svg" alt="Image">
</div>
<div class="container">
<div class="row">
<div class="col-lg-8 offset-lg-2">
<div class="promo-content  text-center">
<h2 class="text-white">Need a publication in high impact factor journal?</h2>
<p class="text-white">Rejection of a manuscript, despite relentless effort in shaping the same, is very disheartening to any medical researcher. Our team offers complete journal publication support services including journal selection, statistical analysis, manuscript re-writing, editing, and formatting as per the journal’s author instruction. At the advance level, we help in re-submission to any alternate journal, manuscript tracking and to revise the manuscript based on the final acceptance requirements.</p>
<a href="aboutus.php" class="btn v1"> <i class="ri-eye-line"></i> View More</a>
</div>
</div>
</div>
</div>
</div>


<?php include('layout/services.php'); ?>


<!-- <div class="promo-video-wrap"> 
	<div class="container">
	<div class="prom-dot-shape md-none">
	<img src="assets/img/promo/dot-shape-3.png" alt="Image">
	</div>
	<div class="prom-circle-shape md-none">
	<img src="assets/img/promo/promo-circle-shape.svg" alt="Image">
	</div>
	<div class="row">
	<div class="col-lg-10 offset-lg-1">
	<div class="promo-video-bg bg-f promo-bg-1">
	<a class="video-play circle style1" href="https://www.youtube.com/watch?v=xHegpKx61eE"> <i class="ri-play-fill"></i> </a>
	</div>
	</div>
	</div>
	</div>
</div>-->


<!-- <section class="discount-wrap"> 
	<div class="discount-item-wrap bg-f promo-bg-6">
	<div class="discount-item-left bg-f promo-bg-3">
	<div class="overlay bg-midnight op-9"></div>
	<div class="content-wrap style1">
	<h2>Up-to 15% Discount Offer For Joining Today</h2>
	<p>The user can create dummy content in word paragraph list items and proposals. Depending on your requirement, a user can fit any of these formats in their project, which adds a lot of conveniences.</p>
	<a href="login.html" class="btn v1"> <i class="ri-logout-circle-r-line"></i> Register Now</a>
	</div>
	</div>
	<div class="promo-circle-shape md-none">
	<img src="assets/img/promo/promo-circle-shape.svg" alt="Image">
	</div>
	</div>
</section>-->











<?php include('layout/footer.php'); ?>

</div>


<a href="#" class="back-to-top bounce"><i class="las la-arrow-up"></i></a>


<!-- <script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script> -->

<script src="assets/js/jquery.min.js"></script>

<script src="assets/js/jquery-ui.min.js"></script>

<script src="assets/js/bootstrap.bundle.min.js"></script>
<script src="assets/js/bootstrap-validator.js"></script>
<script src="assets/js/form-validation.js"></script>

<script src="assets/js/swiper-min.js"></script>

<script src="assets/js/jquery-magnific-popup.js"></script>

<script src="assets/js/countdown.js"></script>

<!-- <script src="assets/js/jquery-nice-select.html"></script> -->

<script src="assets/js/main.js"></script>
</body>

</html>