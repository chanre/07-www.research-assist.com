<footer class="footer-wrap style1 bg-heath">
<div class="footer-top pt-100 pb-70">
<div class="container">
<div class="row ">
<div class="col-lg-4 col-md-6 col-sm-6">
<div class="footer-widget">
<a href="index.html" class="footer-logo"><img src="assets/img/logo24.jpg" alt="Image"></a>
<p>A Concept to publication</p>
<h4 class="footer-widget-title">Newsletter</h4>
<form action="#" class="newsletter-wrap">
<div class="form-group">
<input type="text" placeholder="Your Email Address ..">
<button type="submit" class="submit-btn"> <i class="lab la-telegram-plane"></i></button>
</div>
</form>
<!-- <p>We Never Spam!</p> -->
</div>
</div>
<div class="col-lg-2 col-md-6 col-sm-6">
<div class="footer-widget ml-30">
<h4 class="footer-widget-title">Support</h4>
<ul class="footer-menu">
<li><a href="#">Disclaimer</a></li>
<li><a href="contact.php">Contact Us</a></li>
<li><a href="aboutus.php">About</a></li>
<li><a href="privacy-policy.php">Privacy Policy</a></li>
<li><a href="#">Terms & Conditions</a></li>
</ul>
</div>
</div>
<div class="col-lg-2 offset-lg-1 col-md-6 col-sm-6">
<div class="footer-widget">
<h4 class="footer-widget-title">Important Link</h4>
<ul class="footer-menu">
<li><a href="english-editing.php">English language editing</a></li>
<li><a href="research-design.php">Research Design</a></li>
<li><a href="manuscript-editing.php">Manuscript Editing</a></li>
<li><a href="manuscript-writing.php">Manuscript Writing</a></li>
<li><a href="clinical-trial-design.php">Clinical Trial Design</a></li>
</ul>
</div>
</div>
<div class="col-lg-3 col-md-6 col-sm-6">
<div class="footer-widget">
<h4 class="footer-widget-title">Contact Us</h4>
<ul class="footer-contact-address">
<li><a href="tel:8042516636"> <i class="ri-phone-line"></i> +91 80-42516636</a></li>
<li> <i class="ri-mail-send-fill"></i> <a href="#">marketing@research-assist.com</a></li>
<li> <i class="ri-earth-fill"></i> <a href="https://research-assist.com/">www.research-assist.com</a></li>
</ul>
<div class="share-on">
<span>Follow Us :</span>
<ul class="social-profile">
<li><a target="_blank" href="https://facebook.com/"><i class="ri-facebook-fill"></i> </a></li>
 <li><a target="_blank" href="https://twitter.com/"> <i class="ri-twitter-fill"></i> </a></li>
<li><a target="_blank" href="https://linkedin.com/"> <i class="ri-linkedin-fill"></i> </a></li>
<li><a target="_blank" href="https://instagram.com/"> <i class="ri-instagram-line"></i> </a></li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="footer-bottom">
<div class="container">
<div class="row align-items-center">
<div class="col-lg-12 col-md-8">
<div class="copyright-text">
<p>Copyright <span class="las la-copyright"></span> 2021-22 by Research Assist </p>
</div>
</div>
</div>
</div>
</div>
</footer>



