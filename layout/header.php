<header class="header-wrap style1">
		<div class="header-top">
			<div class="container">
			<div class="row align-items-center">
			<div class="col-lg-6">
			<div class="header-top-left">
			<div class="close-header-top xl-none">
			<button type="button"><i class="las la-times"></i></button>
			</div>
			<div class="header-contact">
			<p><a href="https://goo.gl/maps/TFkhqCgzYPxkGLgCA"><i class="ri-map-pin-fill"></i> 1st Block, Rajajinagar, Bangalore</a> </p>
			</div>
			<div class="header-contact">
			<a href="#"> <i class="ri-mail-send-line"></i> <span class="__cf_email__" data-cfemail="70191e161f3015131f05025e131f1d">marketing@research-assist.com</span></a>
			</div>
			</div>
			</div>
			<div class="col-lg-6">
			<div class="header-top-right">
		
			<div class="header-social">
			<span>Follow us :</span>
			<ul class="social-profile style3">
			<li><a target="_blank" href="https://facebook.com/"><i class="ri-facebook-fill"></i> </a></li>
			<li><a target="_blank" href="https://linkedin.com/"> <i class="ri-linkedin-fill"></i> </a></li>
			 <li><a target="_blank" href="https://twitter.com/"> <i class="ri-twitter-fill"></i> </a></li>
			<li><a target="_blank" href="https://instagram.com/"> <i class="ri-instagram-line"></i> </a></li>
			</ul>
			</div>
			<!-- <a href="login.html" class="link style3"> Login/Register</a> -->
			</div>
			</div>
			<div class="col-lg-6 xl-none">
			<div class="header-search">
			<form action="#">
			<div class="form-group">
			<input type="search" placeholder="Search Here ...">
			<button type="submit"> <i class="ri-search-eye-line"></i> </button>
			</div>
			</form>
			</div>
			</div>
			</div>
			</div>
		</div>
		<div class="header-bottom">
			<div class="container">
			<div class="row align-items-center">
			<div class="col-lg-2 col-md-4 col-5 order-lg-1 order-md-1 order-1">
			<div class="logo">
			<a href="index.php"><img src="assets/img/logo24.jpg" alt="Image"></a>
			</div>
			</div>
			<div class="col-lg-8 col-md-5 col-7 order-lg-2 order-md-3 order-3 mr-auto">
			<div class="main-menu-wrap style1">
			<div class="menu-close xl-none">
			<a href="javascript:void(0)"><i class="las la-times"></i></a>
			</div>
			<div id="menu" class="text-center">
			<ul class="main-menu ">
			<li class="">
			<a class="active" href="index.php">Home</a>
			</li>
			<li class="">
			<a href="aboutus.php">About Us</a>
			</li>
			<li class="has-children">
			<a href="#">Services</a>
			<ul class="sub-menu">
			<li>
				<a  href="english-editing.php">English Language Editing</a></li>
                     <li class="divider"></li>
                                       <li><a  href="research-design.php">Research Designing</a></li>
                     <li class="divider"></li>
                                       <li><a  href="manuscript-editing.php">Manuscript Editing</a></li>
                     <li class="divider"></li>
                                       <li><a  href="manuscript-writing.php">Manuscript Writing</a></li>
                     <li class="divider"></li>
                                       <li><a  href="clinical-trial-design.php">Clinical Trial Design</a></li>
                     <li class="divider"></li>
                                       <li><a href="figure-preparation.php">Figure Preparation</a></li>
                     <li class="divider"></li>
                     <li><a href="data-management.php">Data Management</a></li>
                      <li class="divider"></li>
                       <li><a href="#">Utility & Tools</a></li>
                     <li class="divider"></li>
                     <li><a href="https://iradatabaseard.in/iracovid/demo/">Preview Demo</a></li>
			</ul>
			</li>
			<li class="has-children">
			<a href="#">Academics</a>
			<ul class="sub-menu">
			 <li><a  href="regular-bio-static.php">Regular Biostatistics program</a></li>
                     <li class="divider"></li>
                                       <li><a  href="https://indiainflammation.org/register.php">SIR Lecture Series</a></li>
                                       <li class="divider"></li>
                     <li><a href="rprogramming.html">Virtual Training Program on R</a></li>
                     <li class="divider"></li>
                                       <li><a  href="register.php">High-end Statistics Training Programme for Doctors</a></li>
                     <li class="divider"></li>
			</ul>
			</li>
			<li><a href="payment.php">Payment</a></li>
			<li><a href="upload-manuscript.php">Upload Manuscript</a></li>
			<li><a href="contact.php">Contact</a></li>
			</ul>
			</div>
			</div>

			<div class="mobile-bar-wrap">
			
			
			
			<div class="mobile-menu xl-none">
			<a href='javascript:void(0)'><i class="ri-menu-line"></i></a>
			</div>
			</div>
			</div>
			<div class="col-lg-2 col-md-3  order-lg-3 order-md-2">
			<div class="header-menu-wrap">
			
			
			</div>
			</div>
			</div>
			</div>
		</div>
	</header>


<!--
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5fa0d74ce019ee7748f033a5/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
End of Tawk.to Script-->

	


