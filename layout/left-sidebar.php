<div class="col-xl-3 col-lg-3 col-md-3 order-xl-1 order-lg-2 order-md-2 order-2">
<div class="sidebar">


<div class="sidebar-widget categories box">
<h4>Other Links</h4>
<div class="category-box">
	<ul>
		<li><a  href="english-editing.php">English Language Editing</a></li>
                     <li class="divider"></li>
                                       <li><a  href="research-design.php">Research Designing</a></li>
                     <li class="divider"></li>
                                       <li><a  href="manuscript-editing.php">Manuscript Editing</a></li>
                     <li class="divider"></li>
                                       <li><a  href="manuscript-writing.php">Manuscript Writing</a></li>
                     <li class="divider"></li>
                                       <li><a  href="clinical-trial-design.php">Clinical Trial Design</a></li>
                     <li class="divider"></li>
                                       <li><a href="figure-preparation.php">Figure Preparation</a></li>
                     <li class="divider"></li>
                     <li><a href="data-management.php">Data Management</a></li>
                      <li class="divider"></li>
                       <li><a href="#">Utility & Tools</a></li>
                     <li class="divider"></li>
                     <li><a href="https://iradatabaseard.in/iracovid/demo/">Preview Demo</a></li>
	</ul>
</div>
</div>

</div>
</div>