<section class="course-wrap pt-100 pb-70">
		<div class="container">
		<div class="row">
		<div class="col-lg-12">
		<div class="section-title style1 text-center mb-40">
		<span>Our Services</span>
		<h2></h2>
		</div>
		</div>
		</div>
		<div class="row justify-content-md-center">
			<div class="col-xl-3 col-lg-4 col-md-6">
				<div class="course-card style1">
					<div class="course-img">
					
					</div>
				<div class="course-info">
				<h3><a href="english-editing.php">English language editing</a></h3>
				<div class="course-rating">
					<ul>
					<li> <i class="ri-star-fill"></i></li>
					<li> <i class="ri-star-fill"></i></li>
					<li> <i class="ri-star-fill"></i></li>
					<li> <i class="ri-star-fill"></i></li>
					<li> <i class="ri-star-line"></i> </li>
					</ul>
				<!-- <span>4 Reviews</span> -->
				</div>
				<p>Every year hundreds of papers are rejected due to English language editing requirements. we can help to getting your research published.</p>
				</div>
					<div class="course-metainfo">
					<div class="course-metainfo-left">
				
					</div>
					<div class="course-metainfo-right">
					<div class="price-tag">
					<p></p>
					</div>
					 </div>
					</div>
				</div>
			</div>


			<div class="col-xl-3 col-lg-4 col-md-6">
				<div class="course-card style1">
					<div class="course-img">
					
					</div>
				<div class="course-info">
				<h3><a href="research-design.php">Research Designing</a></h3>
				<div class="course-rating">
					<ul>
					<li> <i class="ri-star-fill"></i></li>
					<li> <i class="ri-star-fill"></i></li>
					<li> <i class="ri-star-fill"></i></li>
					<li> <i class="ri-star-fill"></i></li>
					<li> <i class="ri-star-line"></i> </li>
					</ul>
				<!-- <span>4 Reviews</span> -->
				</div>
				<p>If you have a research question or concept in mind, We can help you in designing the research and data analysis.</p>
				</div>
					<div class="course-metainfo">
					<div class="course-metainfo-left">
				
					</div>
					<div class="course-metainfo-right">
					<div class="price-tag">
					<p></p>
					</div>
					 </div>
					</div>
				</div>
			</div>

			<div class="col-xl-3 col-lg-4 col-md-6">
				<div class="course-card style1">
					<div class="course-img">
					
					</div>
				<div class="course-info">
				<h3><a href="manuscript-editing.php">Manuscript Editing</a></h3>
				<div class="course-rating">
					<ul>
					<li> <i class="ri-star-fill"></i></li>
					<li> <i class="ri-star-fill"></i></li>
					<li> <i class="ri-star-fill"></i></li>
					<li> <i class="ri-star-fill"></i></li>
					<li> <i class="ri-star-line"></i> </li>
					</ul>
				<!-- <span>4 Reviews</span> -->
				</div>
				<p>Accurate and clear presentation of the information is important for accepting the manuscript by international journals.</p>
				</div>
					<div class="course-metainfo">
					<div class="course-metainfo-left">
				
					</div>
					<div class="course-metainfo-right">
					<div class="price-tag">
					<p></p>
					</div>
					 </div>
					</div>
				</div>
			</div>

			<div class="col-xl-3 col-lg-4 col-md-6">
				<div class="course-card style1">
					<div class="course-img">
					
					</div>
				<div class="course-info">
				<h3><a href="manuscript-writing.php">Manuscript Writing</a></h3>
				<div class="course-rating">
					<ul>
					<li> <i class="ri-star-fill"></i></li>
					<li> <i class="ri-star-fill"></i></li>
					<li> <i class="ri-star-fill"></i></li>
					<li> <i class="ri-star-fill"></i></li>
					<li> <i class="ri-star-line"></i> </li>
					</ul>
				<!-- <span>4 Reviews</span> -->
				</div>
				<p>Rejection of a manuscript, despite relentless effort in shaping the same, is very disheartening to any medical researcher.</p>
				</div>
					<div class="course-metainfo">
					<div class="course-metainfo-left">
				
					</div>
					<div class="course-metainfo-right">
					<div class="price-tag">
					<p></p>
					</div>
					 </div>
					</div>
				</div>
			</div>


			<div class="col-xl-3 col-lg-4 col-md-6">
				<div class="course-card style1">
					<div class="course-img">
					
					</div>
				<div class="course-info">
				<h3><a href="clinical-trial-design.php">Clinical Trial Design</a></h3>
				<div class="course-rating">
					<ul>
					<li> <i class="ri-star-fill"></i></li>
					<li> <i class="ri-star-fill"></i></li>
					<li> <i class="ri-star-fill"></i></li>
					<li> <i class="ri-star-fill"></i></li>
					<li> <i class="ri-star-line"></i> </li>
					</ul>
				<!-- <span>4 Reviews</span> -->
				</div>
				<p>We offer clinical trial design services from concept to report.Our experts take up preparation of all types of documents </p>
				</div>
					<div class="course-metainfo">
					<div class="course-metainfo-left">
				
					</div>
					<div class="course-metainfo-right">
					<div class="price-tag">
					<p></p>
					</div>
					 </div>
					</div>
				</div>
			</div>


			<div class="col-xl-3 col-lg-4 col-md-6">
				<div class="course-card style1">
					<div class="course-img">
					
					</div>
				<div class="course-info">
				<h3><a href="figure-preparation.php">Figure Preparation</a></h3>
				<div class="course-rating">
					<ul>
					<li> <i class="ri-star-fill"></i></li>
					<li> <i class="ri-star-fill"></i></li>
					<li> <i class="ri-star-fill"></i></li>
					<li> <i class="ri-star-fill"></i></li>
					<li> <i class="ri-star-line"></i> </li>
					</ul>
				<!-- <span>4 Reviews</span> -->
				</div>
				<p>Research assist team can help to ensure editorial excellence by functioning as an intermediary between publishers and authors. </p>
				</div>
					<div class="course-metainfo">
					<div class="course-metainfo-left">
				
					</div>
					<div class="course-metainfo-right">
					<div class="price-tag">
					<p></p>
					</div>
					 </div>
					</div>
				</div>
			</div>

			<div class="col-xl-3 col-lg-4 col-md-6">
				<div class="course-card style1">
					<div class="course-img">
					
					</div>
				<div class="course-info">
				<h3><a href="data-management.php">Data Management</a></h3>
				<div class="course-rating">
					<ul>
					<li> <i class="ri-star-fill"></i></li>
					<li> <i class="ri-star-fill"></i></li>
					<li> <i class="ri-star-fill"></i></li>
					<li> <i class="ri-star-fill"></i></li>
					<li> <i class="ri-star-line"></i> </li>
					</ul>
				<!-- <span>4 Reviews</span> -->
				</div>
				<p>We assist top research institute in India as well as other countries for developing database registry and related services. </p>
				</div>
					<div class="course-metainfo">
					<div class="course-metainfo-left">
				
					</div>
					<div class="course-metainfo-right">
					<div class="price-tag">
					<p></p>
					</div>
					 </div>
					</div>
				</div>
			</div>

			<div class="col-xl-3 col-lg-4 col-md-6">
				<div class="course-card style1">
					<div class="course-img">
					
					</div>
				<div class="course-info">
				<h3><a href="#">Utility & Tools</a></h3>
				<div class="course-rating">
					<ul>
					<li> <i class="ri-star-fill"></i></li>
					<li> <i class="ri-star-fill"></i></li>
					<li> <i class="ri-star-fill"></i></li>
					<li> <i class="ri-star-fill"></i></li>
					<li> <i class="ri-star-line"></i> </li>
					</ul>
				<!-- <span>4 Reviews</span> -->
				</div>
				<p>Coming soon..</p>
				</div>
					<div class="course-metainfo">
					<div class="course-metainfo-left">
				
					</div>
					<div class="course-metainfo-right">
					<div class="price-tag">
					<p></p>
					</div>
					 </div>
					</div>
				</div>
			</div>
	
		</div>
		</div>
</section>