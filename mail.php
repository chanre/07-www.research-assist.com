<?php
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer; 
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';


if (isset($_POST['submit'])) {

    $name=$_POST['name'];
    $email = $_POST['email'];
    $message=$_POST['message'];
    $subject=$_POST['subject'];
    $mobile = $_POST['mobile'];

    $mail = new PHPMailer(true);// Passing `true` enables exceptions
    // $code = uniqid(true); // true for more uniqueness 
    // $query = mysqli_query($con,"INSERT INTO resetpasswords (code, email) VALUES('$code','$emailTo')"); 
    
    try {
        //Server settings
        $mail->SMTPDebug = 0;     // Enable verbose debug output, 1 for produciton , 2,3 for debuging in devlopment 
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'webdesigner@chanrejournals.com';        // SMTP username
        $mail->Password = '*meghalaya2';                          // SMTP password
        // $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
        // $mail->Port = 587;   // for tls                                 // TCP port to connect to
        $mail->Port = 465;

        //Recipients
        $mail->setFrom($email, $name); // from who? 
        $mail->addAddress('marketing@research-assist.com', 'Vijayendra');     // Add a recipient
        $mail->addCC('webdesigner@chanrejournals.com');
        $mail->addReplyTo($email, 'Reply');
        // $mail->addCC('cc@example.com');
        // $mail->addBCC('bcc@example.com');

        //Content
        // this give you the exact link of you site in the right page 
        // if you are in actual web server, instead of http://" . $_SERVER['HTTP_HOST'] write your link 
        

        // $url = "https://" . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']). "/resetPassword.php?code=$code"; 
    $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = $subject;
        $mail->Body    = "
                    <table border='2'>
                    <tr>
                    <td>Name</td>
                    <td>$name</td>
                    </tr>

                    <tr>
                    <td>Contact</td>
                    <td>$mobile</td>
                    </tr>

                    <tr>
                    <td>Email</td>
                    <td>$email</td>
                    </tr>

                    <tr>
                    <td>Subject</td>
                    <td>$subject</td>
                    </tr>

                    <tr>
                    <td>Message</td>
                    <td>$message</td>
                    </tr>
                    </table> ";
        $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

        // to solve a problem 
        $mail->SMTPOptions = array(
            'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
            )
        );


        $mail->send();
        echo 'Message has been sent';
    } catch (Exception $e) {
        echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
    }

    exit(); // to stop user from submitting more than once 
}

?>