<!DOCTYPE html>
<html lang="zxx">
<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title>Payment - Research Assist</title>

<link rel="preconnect" href="https://fonts.gstatic.com/">
<link href="https://fonts.googleapis.com/css2?family=PT+Serif:wght@400;700&amp;family=Poppins:wght@200;300;400;500;600;700;800&amp;display=swap" rel="stylesheet">

<link rel="shortcut icon" href="assets/img/favicon.png" type="image/png">

<link href="assets/css/bootstrap.min.css" rel="stylesheet">

<link href="assets/css/jquery-ui-min.css" rel="stylesheet">

<link href="assets/css/line-awesome.min.css" rel="stylesheet">
<link href="assets/css/remixicon.css" rel="stylesheet">

<link href="assets/css/animate.min.css" rel="stylesheet">

<link href="assets/css/swiper-min.css" rel="stylesheet">

<link href="assets/css/magnific-popup.css" rel="stylesheet">

<link href="assets/css/style.css" rel="stylesheet">
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-TB1EHXL2BC"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-TB1EHXL2BC');
</script>
</head>
<body>

<div class="preloader js-preloader">
<img src="assets/img/preloader.gif" alt="Image">
</div>


<div class="page-wrapper">
<?php include('layout/header.php'); ?>


<section class="breadcrumb-wrap bg-f br-bg-1">
<div class="overlay op-6 bg-black"></div>
<div class="container">
<div class="row">
<div class="col-xl-8 offset-xl-2 col-lg-10 offset-lg-1 col-md-10 offset-md-1">
<div class="breadcrumb-title">
<h2>Payment</h2>
<ul class="breadcrumb-menu">
<li><a href="index.php">Home </a></li>
<li>Payment</li>
</ul>
</div>
</div>
</div>
</div>
</section>


<section class="Login-wrap pt-20 pb-100">
<div class="container">
<div class="col-xl-6 offset-xl-3 col-lg-8 offset-lg-2 col-md-10 offset-md-1">
<div class="login-form">
<div class="login-header bg-blue">
<h2 class="text-center mb-0">Payment</h2>
</div>
<div class="login-body">
<form class="form-wrap" action="#">
<div class="col-md-9 col-sm-8 col-xs-12">
                  <div class="row">
                     <h4>Pricing</h4>
                     <p>Email us your document in MS Word format to receive a free cost estimate for our services. All of our services can be customized to meet your specific needs. We also offer all services at discounted rates for organizations, universities, and other non-profit institutions who wish to publish scientific content purely for educational and research purposes (non-commercial)</p>
                     <p></p>
                  </div>
                  <!--<img src="img/background/footer.png" alt="">-->
                  <h6>PAYMENT INFORMATION</h6>
                  <h6>EFT (Electronic Funds Transfer)</h6>
                  <p>Make your EFT payment at your bank or via the Internet using the following account details</p>
                     <p>Beneficiary Name: CHANRE HEALTH CARE &amp; RESEARCH PVT LTD<br>
                      Bank Name:HDFC BANK<br>
                      Account Number: 50200061249352<br>
                      MICR Number: 560240003<br>
                      IFSC Code: HDFC0000041<br>
                      Branch: #47, MARGOSA ROAD, 15TH CROSS MALLESWARAM, BANGALORE - 560003<br>
                  </p><p>Please send a remittance advice by email to <b>marketing@chanrejournals.com</b> clearly identifying your payment and detailing invoice information.</p>    
                  <h6>CHEQUE / DD</h6>
                  <p>For all the prepayment customer Cheques/DD to be made payable to CHANRE HEALTH CARE &amp; RESEARCH PVT LTD.,.</p>
                  <p>Please courier your Cheques / DD to the below Address:<br>

                   ChanRe Rheumatology and Immunology Center &amp; Research<br>

                   No. 414/65, 20th Main,<br>

                   West of Chord road,1st Block, Rajajinagar, Bangalore-10<br>

                   Ph:080 - 42516699, Telefax:080 - 42516600</p>
               </div>
</form>
</div>
</div>
</div>
</div>
</section>


<?php include('layout/footer.php'); ?>

</div>


<a href="#" class="back-to-top bounce"><i class="las la-arrow-up"></i></a>


<script src="assets/js/jquery.min.js"></script>

<script src="assets/js/jquery-ui.min.js"></script>

<script src="assets/js/bootstrap.bundle.min.js"></script>
<script src="assets/js/bootstrap-validator.js"></script>
<script src="assets/js/form-validation.js"></script>

<script src="assets/js/swiper-min.js"></script>

<script src="assets/js/jquery-magnific-popup.js"></script>

<script src="assets/js/countdown.js"></script>

<script src="assets/js/main.js"></script>
</body>
</html>