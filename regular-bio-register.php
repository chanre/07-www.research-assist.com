<?php
session_start();
include('config.php');


	if(isset($_POST['submit']))
{
   $name=$_POST['name'];
	 $designation=$_POST['designation'];
	 $kmcno=$_POST['kmcno'];
	 $paddress=$_POST['paddress'];
	 $qualification=$_POST['qualification'];
	 $correspondence=$_POST['correspondence'];
	 $mobile=$_POST['mobile'];
	 $email=$_POST['email'];
	 
    $query=mysqli_query($con,"Insert into regularbio (name,designation,kmcno,paddress,qualification,correspondence,mobile,email) 
	values('$name','$designation','$kmcno','$paddress','$qualification','$correspondence','$mobile','$email')");

	if($query)
{
echo "<script> alert('Your request is successfully submitted!!')</script>";
}
else{
echo "<script> alert('Your request can not processed.. Please try again!!')</script>";
} 
	
	
	
	
}
    ?>
<!DOCTYPE html>
<html lang="zxx">
<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title>Regular Biostatic Registration - Research Assist</title>

<link rel="preconnect" href="https://fonts.gstatic.com/">
<link href="https://fonts.googleapis.com/css2?family=PT+Serif:wght@400;700&amp;family=Poppins:wght@200;300;400;500;600;700;800&amp;display=swap" rel="stylesheet">

<link rel="shortcut icon" href="assets/img/favicon.png" type="image/png">

<link href="assets/css/bootstrap.min.css" rel="stylesheet">

<link href="assets/css/jquery-ui-min.css" rel="stylesheet">

<link href="assets/css/line-awesome.min.css" rel="stylesheet">
<link href="assets/css/remixicon.css" rel="stylesheet">

<link href="assets/css/animate.min.css" rel="stylesheet">

<link href="assets/css/swiper-min.css" rel="stylesheet">

<link href="assets/css/magnific-popup.css" rel="stylesheet">

<link href="assets/css/style.css" rel="stylesheet">

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-TB1EHXL2BC"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-TB1EHXL2BC');
</script>
</head>
<body>

<div class="preloader js-preloader">
<img src="assets/img/preloader.gif" alt="Image">
</div>


<div class="page-wrapper">
<?php include('layout/header.php'); ?>


<section class="breadcrumb-wrap bg-f br-bg-1">
<div class="overlay op-6 bg-black"></div>
<div class="container">
<div class="row">
<div class="col-xl-8 offset-xl-2 col-lg-10 offset-lg-1 col-md-10 offset-md-1">
<div class="breadcrumb-title">
<h2>Register</h2>
<ul class="breadcrumb-menu">
<li><a href="index.php">Home </a></li>
<li>Register</li>
</ul>
</div>
</div>
</div>
</div>
</section>


<section class="Login-wrap pt-100 pb-100">
<div class="container">
<div class="col-xl-6 offset-xl-3 col-lg-8 offset-lg-2 col-md-10 offset-md-1">
<div class="login-form">
<div class="login-header bg-blue">
<h2 class="text-center mb-0">Register</h2>
</div>
<div class="login-body">
<form class="form-wrap" action="#" method="post">
<div class="row">
<div class="col-lg-12">
<div class="form-group">
<label for="name">Full Name</label>
<input id="name" name="name" type="text" placeholder="Full Name" required>
</div>
</div>
<div class="col-lg-12">
<div class="form-group">
<label for="name">Email Address</label>
<input id="email" name="email" type="email" placeholder="Email Address*" required>
</div>
</div>
<div class="col-lg-12">
<div class="form-group">
<label for="phone">Phone Number</label>
<input id="phone" name="phone" type="number" placeholder="Phone Number">
</div>
</div>

<div class="col-lg-12">
<div class="form-group">
<label for="Designation">Designation</label>
<input id="name" name="designation" type="text" placeholder="Designation" required>
</div>
</div>

<div class="col-lg-12">
<div class="form-group">
<label for="MNC">Medical Council Number (Non-UG)</label>
<input id="kmc" name="kmcno" type="text" placeholder="KMC No">
</div>
</div>



<div class="col-lg-12">
<div class="form-group">
<label for="Present Address">Present Address</label>
<textarea name="paddress" type="text" placeholder="Present Address"></textarea>
</div>
</div>

<div class="col-lg-12">
<div class="form-group">
<label for="Permanent Address">Permanent Address</label>
<textarea name="correspondence" type="text" placeholder="Permanent Address" ></textarea>

</div>
</div>

<div class="col-lg-12">
<div class="form-group">
<label for="qualification">Education Qualification</label>
<input id="kmc" name="qualification" type="text" placeholder="Educational Qualification">
</div>
</div>

<div class="col-lg-12">
<div class="form-group">
<button class="btn v6" name="submit" type="submit">Register</button>
</div>
</div>

</div>
</form>
</div>
</div>
</div>
</div>
</section>


<?php include('layout/footer.php'); ?>

</div>


<a href="#" class="back-to-top bounce"><i class="las la-arrow-up"></i></a>


<script src="assets/js/jquery.min.js"></script>

<script src="assets/js/jquery-ui.min.js"></script>

<script src="assets/js/bootstrap.bundle.min.js"></script>
<script src="assets/js/bootstrap-validator.js"></script>
<script src="assets/js/form-validation.js"></script>

<script src="assets/js/swiper-min.js"></script>

<script src="assets/js/jquery-magnific-popup.js"></script>

<script src="assets/js/countdown.js"></script>

<script src="assets/js/main.js"></script>
</body>
</html>