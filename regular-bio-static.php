<!DOCTYPE html>
<html lang="zxx">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title>Regular Biostatic Registration - Research Assist</title>

<link rel="preconnect" href="https://fonts.gstatic.com/">
<link href="https://fonts.googleapis.com/css2?family=PT+Serif:wght@400;700&amp;family=Poppins:wght@200;300;400;500;600;700;800&amp;display=swap" rel="stylesheet">

<link rel="shortcut icon" href="assets/img/favicon.png" type="image/png">

<link href="assets/css/bootstrap.min.css" rel="stylesheet">

<link href="assets/css/jquery-ui-min.css" rel="stylesheet">

<link href="assets/css/line-awesome.min.css" rel="stylesheet">
<link href="assets/css/remixicon.css" rel="stylesheet">

<link href="assets/css/animate.min.css" rel="stylesheet">

<link href="assets/css/swiper-min.css" rel="stylesheet">

<link href="assets/css/magnific-popup.css" rel="stylesheet">

<link href="assets/css/style.css" rel="stylesheet">
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-TB1EHXL2BC"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-TB1EHXL2BC');
</script>
</head>
<body>

<div class="preloader js-preloader">
<img src="assets/img/preloader.gif" alt="Image">
</div>


<div class="page-wrapper">

<?php include('layout/header.php'); ?>

<section class="breadcrumb-wrap bg-f br-bg-1">
<div class="overlay op-6 bg-black"></div>
<div class="container">
<div class="row">
<div class="col-xl-8 offset-xl-2 col-lg-10 offset-lg-1 col-md-10 offset-md-1">
<div class="breadcrumb-title">
<h2>Regular Biostatic Program </h2>
</div>
</div>
</div>
</div>
</section>

<div class="post-details pt-100 pb-100">
<div class="container">
<div class="row gx-5">
<?php include('layout/left-sidebar.php'); ?>
<div class="col-xl-9 col-lg-9 col-md-9 order-xl-2 order-lg-1 order-md-1 order-1">
<div class="content-wrapper">
<article>
<div class="post-content ">
<div class="post-img">
<img src="assets/img/academic/1.png" alt="Image">
<img src="assets/img/academic/r2.jpg" alt="Image">
</div>

<p>For any queries contact us on +91 9535056289, +91 7618775276</p> <br>
          <p>Please choose whether you are a student or factulty. After choosing you will get a payment form once you are done with payment you will get the registration link</p>
     <div class="col-md-4"></div>
     <div class="col-md-4">
      <!-- <form><script src="https://checkout.razorpay.com/v1/payment-button.js" data-payment_button_id="pl_HPdmq5t1NwIRLU" async> </script> </form> -->
       <!-- <form><script src="https://checkout.razorpay.com/v1/payment-button.js" data-payment_button_id="pl_HPdovd6gDuhO9n" async> </script> </form> -->


 								


</div>
</article>

</div>
</div>
</div>
</div>
</div>


<?php include('layout/footer.php'); ?>

</div>


<a href="#" class="back-to-top bounce"><i class="las la-arrow-up"></i></a>


<script src="assets/js/jquery.min.js"></script>

<script src="assets/js/jquery-ui.min.js"></script>

<script src="assets/js/bootstrap.bundle.min.js"></script>
<script src="assets/js/bootstrap-validator.js"></script>
<script src="assets/js/form-validation.js"></script>

<script src="assets/js/swiper-min.js"></script>

<script src="assets/js/jquery-magnific-popup.js"></script>

<script src="assets/js/countdown.js"></script>

<script src="assets/js/main.js"></script>
</body>
</html>