<!DOCTYPE html>
<html lang="zxx">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title>Research designing and statistical analysis - Research Assist</title>

<link rel="preconnect" href="https://fonts.gstatic.com/">
<link href="https://fonts.googleapis.com/css2?family=PT+Serif:wght@400;700&amp;family=Poppins:wght@200;300;400;500;600;700;800&amp;display=swap" rel="stylesheet">

<link rel="shortcut icon" href="assets/img/favicon.png" type="image/png">

<link href="assets/css/bootstrap.min.css" rel="stylesheet">

<link href="assets/css/jquery-ui-min.css" rel="stylesheet">

<link href="assets/css/line-awesome.min.css" rel="stylesheet">
<link href="assets/css/remixicon.css" rel="stylesheet">

<link href="assets/css/animate.min.css" rel="stylesheet">

<link href="assets/css/swiper-min.css" rel="stylesheet">

<link href="assets/css/magnific-popup.css" rel="stylesheet">

<link href="assets/css/style.css" rel="stylesheet">
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-TB1EHXL2BC"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-TB1EHXL2BC');
</script>
</head>
<body>

<div class="preloader js-preloader">
<img src="assets/img/preloader.gif" alt="Image">
</div>


<div class="page-wrapper">

<?php include('layout/header.php'); ?>

<section class="breadcrumb-wrap bg-f br-bg-1">
<div class="overlay op-6 bg-black"></div>
<div class="container">
<div class="row">
<div class="col-xl-8 offset-xl-2 col-lg-10 offset-lg-1 col-md-10 offset-md-1">
<div class="breadcrumb-title">
<h2>Research Designing and Statistical Analysis</h2>
</div>
</div>
</div>
</div>
</section>

<div class="post-details pt-100 pb-100">
<div class="container">
<div class="row gx-5">
<?php include('layout/left-sidebar.php'); ?>
<div class="col-xl-8 col-lg-8 order-xl-2 order-lg-1 order-md-1 order-1">
<div class="content-wrapper">
<article>
<div class="post-content ">
<div class="post-img">
<!-- <img src="assets/img/blog/single-blog.jpg" alt="Image"> -->
</div>
<h2 class="post-subtitle">Research designing and statistical analysis - Research Assist</h2>
<p>If you have a research question or concept in mind, our team of research professionals from diverse therapeutic areas and with expertise in biostatistics, epidemiology, and data management can help you in designing the research and data analysis. Our team utilizes their skills to ensure that the study design is addressing the appropriate clinical questions.

 </p>

<h2 class="post-subtitle">Quality solution for your research needs</h2>
<p>The success of clinical research relies on statistical interpretation of numerical data. Hence, effective collaboration between clinician and statistician is vital during data analysis. Our statistical consultants are with wider knowledge and experience in performing a variety of statistical methods, such as basic descriptive analyses, epidemiological analysis, correlation analysis, cluster analyses, factor analyses, multivariate analyses of variance, multiple regressions, logistic regressions, propensity score analyses, hierarchical linear modeling (HLM), meta-analyses, structural equation modeling (SEM), etc.</p>
<p>The data-driven analytical services offered by our team strive to ensure the accuracy of results by using the best analysis techniques and computing software. Our biostatisticians help you in</p>
<ol>
                                         <li>1. Developing statistical analysis plans, generating tables and figures,and data  coding.</li>
                                        <li>2. Ensuring the accuracy, quality, and efficiency of the results</li>
                                        <li>3. Choosing the appropriate research design to answer your research questions</li>
                                        <li>4. Developing questionnaire for data collection and calculating various validity and reliability measures.</li>
                                      </ol>
 								
<!-- <div class="row gx-4 mt-30 align-items-center">
<div class="col-lg-6">
<div class="post-img">
<img src="assets/img/blog/post-1.jpg" alt="Image">
</div>
</div>
<div class="col-lg-6">
<div class="post-img">
<img src="assets/img/blog/post-2.jpg" alt="Image">
</div>
</div>
</div> -->

</div>
</article>

</div>
</div>
</div>
</div>
</div>


<?php include('layout/footer.php'); ?>

</div>


<a href="#" class="back-to-top bounce"><i class="las la-arrow-up"></i></a>


<script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="assets/js/jquery.min.js"></script>

<script src="assets/js/jquery-ui.min.js"></script>

<script src="assets/js/bootstrap.bundle.min.js"></script>
<script src="assets/js/bootstrap-validator.js"></script>
<script src="assets/js/form-validation.js"></script>

<script src="assets/js/swiper-min.js"></script>

<script src="assets/js/jquery-magnific-popup.js"></script>

<script src="assets/js/countdown.js"></script>

<script src="assets/js/main.js"></script>
</body>
</html>