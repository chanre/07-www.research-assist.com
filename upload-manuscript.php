
<?php
use PHPMailer\PHPMailer\PHPMailer; 
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';
   if(isset($_FILES['image'])){
      $errors= array();
      $file_name = $_FILES['image']['name'];
      $file_size = $_FILES['image']['size'];
      $file_tmp = $_FILES['image']['tmp_name'];
      $file_type = $_FILES['image']['type'];
       $file_name_array = explode('.',$_FILES['image']['name']);
    $file_ext=strtolower(end($file_name_array));
      
      $expensions= array("jpeg","jpg","png","pdf","docx");
      
      if(in_array($file_ext,$expensions)=== false){
         $errors[]="extension not allowed, please choose a PDF, JPEG or PNG file.";
      }
      
      if($file_size > 2097152) {
         $errors[]='File size must be exactly 2 MB';
      }
      
      if(empty($errors)==true) {
         move_uploaded_file($file_tmp,"uploads/".$file_name); //The folder where you would like your file to be saved
         echo "Success";
      }else{
         print_r($errors);
      }
   }

// PHPMailer script below
if(isset($_POST['submit'])){
$email = $_POST['email'] ;
$name = $_POST['name'] ;
$service=$_POST['service'];
$title=$_POST['title'];
$phone = $_POST['phone'] ;
$message = $_POST['message'] ;
// require("phpmailer/PHPMailerAutoload.php");

$mail = new PHPMailer();

$mail->IsSMTP();

$mail->Host = "smtp.gmail.com";

$mail->SMTPAuth = true; 

$mail->Username = "webdesigner@chanrejournals.com"; // SMTP username
$mail->Password = "*meghalaya2"; // SMTP password
$mail->addAttachment("uploads/".$file_name);
$mail->From = $email;
 $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
        // $mail->Port = 587;   // for tls                                 // TCP port to connect to
$mail->Port = 465;//SMTP port
$mail->addAddress('marketing@chanrejournals.com', 'User');
$mail->addCC('webdesigner@chanrejournals.com');
$mail->Subject = "Research Manuscript query!";
$mail->Body ="
Name: $name<br>
Email: $email<br>
Service: $service <br>
Telephone: $phone<br>
Title: $title<br><br><br>

Comments: $message";
$mail->AltBody = $message;

if($mail->Send())        //Send an Email. Return true on success or false on error
 {
  $message = '<div class="alert alert-success">Application Successfully Submitted</div>';
  unlink($path);
 }
 else
 {
  $message = '<div class="alert alert-danger">There is an Error</div>';
 }


/*
if(!$mail->Send())
{
echo "Message could not be sent. <p>";
echo "Mailer Error: " . $mail->ErrorInfo;
exit;
}

echo "<script>alert('Message has been sent')</script>";*/
}
?>
<!DOCTYPE html>
<html lang="zxx">
<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title>Upload Manuscript - Research Assist</title>

<link rel="preconnect" href="https://fonts.gstatic.com/">
<link href="https://fonts.googleapis.com/css2?family=PT+Serif:wght@400;700&amp;family=Poppins:wght@200;300;400;500;600;700;800&amp;display=swap" rel="stylesheet">

<link rel="shortcut icon" href="assets/img/favicon.png" type="image/png">

<link href="assets/css/bootstrap.min.css" rel="stylesheet">

<link href="assets/css/jquery-ui-min.css" rel="stylesheet">

<link href="assets/css/line-awesome.min.css" rel="stylesheet">
<link href="assets/css/remixicon.css" rel="stylesheet">

<link href="assets/css/animate.min.css" rel="stylesheet">

<link href="assets/css/swiper-min.css" rel="stylesheet">

<link href="assets/css/magnific-popup.css" rel="stylesheet">

<link href="assets/css/style.css" rel="stylesheet">
</head>
<body onload="generateCaptcha();">

<div class="preloader js-preloader">
<img src="assets/img/preloader.gif" alt="Image">
</div>


<div class="page-wrapper">
<?php include('layout/header.php'); ?>


<section class="breadcrumb-wrap bg-f br-bg-1">
<div class="overlay op-6 bg-black"></div>
<div class="container">
<div class="row">
<div class="col-xl-8 offset-xl-2 col-lg-10 offset-lg-1 col-md-10 offset-md-1">
<div class="breadcrumb-title">
<h2>Upload Manuscript</h2>
<ul class="breadcrumb-menu">
<li><a href="index.php">Home </a></li>
<li>Upload Manuscript</li>
</ul>
</div>
</div>
</div>
</div>
</section>


<section class="pt-50 ">
<div class="container">
<div class="col-xl-6 offset-xl-3 col-lg-8 offset-lg-2 col-md-10 offset-md-1">
<div>
                              <hr>
                              <div class="row">
                                 <div class="col-sm-12">
                                    <?php print_r($message); ?>
                                    <form class="form-horizontal" onsubmit="return CheckValidCaptcha()" method="post"  enctype="multipart/form-data">
                                        <div class="form-group">
                                          <label class="control-label " for="">Select Service</label>
                                          <div class="col-sm-10">
                                             <select class="form-control small-form-control" name="service" id="tname"  required="required">
                                                <option value="Service not selected">Select Service </option>
                                                <option value="English Language Editing">English Language Editing </option>
                                                <option value="Research Designing">Research Designing</option>
                                                <option value="Manuscript Editing">Manuscript Editing</option>
                                                <option value="Manuscript Writing">Manuscript Writing</option>
                                                <option value="Clinical Trail Design">Clinical Trial Design</option>
                                                <option value="Figure Preparation">Figure Preparation </option>
                                             </select>
                                          </div>
                                       </div>
                                       <div class="form-group">
                                          <label class="control-label col-sm-2" for="title">Title</label>
                                          <div class="col-sm-10">
                                             <input type="text" class="form-control" id="title" placeholder="Enter your title" name="title" required="required">
                                          </div>
                                       </div>
                                       <br>
                                       <div class="form-group uploadfrm">
                                             <label class="control-label col-sm-2" for="file"> Attach File</label>
                                      <div class="col-sm-10">
									  <!--Here use name="fileToUpload" as like upload.php otherwise its should be display like invalid parameter-->
                                        <input id="file" name="image" type="file" />                
                     </div>
                                       </div>
                                    
                                       <div class="form-group">
                                          <label class="control-label col-sm-2" for="tribute_f_name">Name:</label>
                                          <div class="col-sm-10">
                                             <input type="text" class="form-control" id="tribute_f_name"  name="name" placeholder="Enter your good name" required="required">
                                          </div>
                                       </div>
                                      
                                       <div class="form-group">
                                          <label class="control-label col-sm-2" for="email">Email:</label>
                                          <div class="col-sm-10">
                                             <input type='text' name='email' id='email' pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="Enter correct email" placeholder="Enter your email" required/>
                                          </div>
                                       </div>
                                       <div class="form-group">
                                          <label class="control-label col-sm-2" for="phone">Phone:</label>
                                          <div class="col-sm-10">
                                            <input type="text" id="phone" name="phone" required />
                                          </div>
                                       </div>
                                   
                                       <div class="form-group">
                                          <label class="control-label col-sm-2" for="t_description">Description:</label>
                                          <div class="col-sm-10">
                                             <textarea rows="3" class="form-control" id="t_description"  name="message" required="required"></textarea> 
                                          </div>
                                       </div>
                                       <div class="form-group">
                                          <div class="col-lg-6 col-md-6">
                 <div class=" v1">     
                      <input type="text" class="form-control" id="mainCaptcha"  readonly="readonly" style="background-image: linear-gradient(#00adff6b, #3ee6df36); text-align: center;font-size: 18px;" /><img src="assets/img/refresh1.png" onclick="generateCaptcha();">
                      <span id="error" style="color:red"></span>
                      <span id="success" style="color:green"></span><input type="text" id="txtInput" placeholder="Enter the captcha here..." /> 
                                        <span id="error" style="color:red"></span></td>
                                         <span id="success" style="color:green"></span><br>  
                </div> 
        </div>
                                       </div>
                                       <div class="form-group">
                                          <div class="col-sm-offset-2 col-sm-10">
                                             <div class="suscribe-input">
                                 <!--<input type="email" class="email form-control width-80" id="sus_email" placeholder="Type Email">-->
                                 <button type="submit" id="sus_submit" name="submit" class="add-btn btn btn-primary text-center">Upload Document</button>
                              </div>                                          </div>
                            </div>
                          </form>
                        </div>                            
                  </div>
               </div>
</div>
</div>
</section>


<?php include('layout/footer.php'); ?>

</div>


<a href="#" class="back-to-top bounce"><i class="las la-arrow-up"></i></a>


<script src="assets/js/jquery.min.js"></script>

<script src="assets/js/jquery-ui.min.js"></script>

<script src="assets/js/bootstrap.bundle.min.js"></script>
<script src="assets/js/bootstrap-validator.js"></script>
<script src="assets/js/form-validation.js"></script>

<script src="assets/js/swiper-min.js"></script>

<script src="assets/js/jquery-magnific-popup.js"></script>

<script src="assets/js/countdown.js"></script>
<script type="text/javascript">
    function generateCaptcha()
         {
             var alpha = new Array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');
             var i;
             for (i=0;i<5;i++){
               var a = alpha[Math.floor(Math.random() * alpha.length)];
               var b = alpha[Math.floor(Math.random() * alpha.length)];
               var c = alpha[Math.floor(Math.random() * alpha.length)];
               var d = alpha[Math.floor(Math.random() * alpha.length)];
               var e = alpha[Math.floor(Math.random() * alpha.length)];
              }
            var code = a + '' + b + '' + '' + c + '' + d +''+e;
            document.getElementById("mainCaptcha").value = code
          }
          function CheckValidCaptcha(){
             
              var string1 = removeSpaces(document.getElementById('mainCaptcha').value);
              var string2 = removeSpaces(document.getElementById('txtInput').value);
              if (string1 == string2){
         document.getElementById('success').innerHTML = "Captcha is validated Successfully";
                return true;
              }
              else{       
         document.getElementById('error').innerHTML = "Please enter a valid captcha."; 
                return false;
         
              }
          }
          function removeSpaces(string){
            return string.split(' ').join('');
          }
          
</script>
<script src="assets/js/main.js"></script>
</body>
</html>